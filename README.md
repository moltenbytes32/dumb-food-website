# thebutterchickenlady
This is a shitpost-y project that will eventually become something useful

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Deploying a docker container
```
./build-docker.sh
docker-compose up
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
