const path = require('path');
const util = require('util');
const exec = util.promisify(require('child_process').exec);
const fs = require('fs').promises;

const projDir = path.dirname(__dirname);
const srcDir = path.join(projDir, 'server');
const dstDir = path.join(projDir, 'dist');

async function copyWalk(relDirPath) {
  try { await fs.mkdir(path.join(dstDir, relDirPath)) } catch { }
  const dir = await fs.opendir(path.join(srcDir, relDirPath));

  for await (const entry of dir) {
    if (entry.isDirectory()) copyWalk(path.join(relDirPath, entry.name));
    else if (entry.isFile()) fs.copyFile(path.join(srcDir, relDirPath, entry.name), path.join(dstDir, relDirPath, entry.name));
  }
}

copyWalk('');
