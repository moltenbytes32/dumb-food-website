import express from 'express';
import express_bodyParser from 'body-parser';

import api_geocode from './gmaps/geocode';

const app = express();
app.use(express_bodyParser.json());

api_geocode(app);

app.get('/', (req, res) => {
  res.json({
    message: 'Hello World!',
  });
});

const port = process.env.PORT || 8081;
app.listen(port, () => console.log(`Backend Hosted on http://localhost:${port}`));
