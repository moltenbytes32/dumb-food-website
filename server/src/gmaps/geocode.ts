import { Express } from 'express';
import { Client } from '@googlemaps/google-maps-services-js';

const mapsApiKey = process.env.GMAPS_API_KEY as string;
if (typeof mapsApiKey !== 'string') {
  console.error("Please Provide a Google Maps Api Key via GMAPS_API_KEY");
  process.exit(1);
}

const googleMaps = new Client({});

export default function initMapsApi(app: Express) {
  app.get('/api/geocode/:address', async (req, res) => {
    try {
      const response = await googleMaps.geocode({
        params: {
          address: req.params.address,
          key: mapsApiKey,
        },
      });
  
      if (response.data.results.length > 0) {
        const geocodes: {
          name: string,
          location: { lat: number, lng: number },
        }[] = response.data.results.map((r) => ({ name: r.formatted_address, location: r.geometry.location }));
        res.json(geocodes);
      }
      else {
        res.sendStatus(404);
      }
    }
    catch (error) {
      try {
        res.status(500).json(error);
      }
      catch {
        res.status(500).send("Unserializable Error");
      }
    }
  })
}