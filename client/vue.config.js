module.exports = {
  lintOnSave: false,
  devServer: {
    proxy: {
      '^/api': {
        target: 'localhost:8081',
        ws: true,
        changeOrigin: false,
      }
    }
  },
  pages: {
    index: {
      entry: 'src/main.ts'
    }
  },
  configureWebpack: {
    resolve: {
      extensions: [".js", ".ts"],
    }
  }
}
