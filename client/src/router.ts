import * as VueRouter from 'vue-router';

import HomePage from '@/components/HomePage.vue';
import CreateRoom from '@/components/CreateRoom.vue';

const routes = [
  { path: '/', component: HomePage },
  { path: '/rooms/create', component: CreateRoom },
];

const router = VueRouter.createRouter({
  history: VueRouter.createWebHashHistory(),
  routes,
});

export default router;