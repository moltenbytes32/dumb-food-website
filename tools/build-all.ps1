push-location ../client
npm run build
pop-location

push-location ../server
npm run build
pop-location

push-location ..
remove-item -force dist 2> out-null
new-item -type directory dist
copy-item -recurse client/dist/* dist/
copy-item -recurse server/dist/* dist/
pop-location