param([switch]$dist)

if ($dist) { remove-item -force -recurse ../dist 2> $null }
remove-item -force -recurse ../client/dist 2> $null
remove-item -force -recurse ../server/dist 2> $null